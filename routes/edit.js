const express = require("express");
const router = express.Router();
const multer = require("multer");

const Product = require("../models/product");

const storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, "./uploads/");
  },
  filename: function(res, file, callback) {
    callback(null, new Date().getTime() + "-" + file.originalname);
  }
});

const fileFilter = function(req, file, callback) {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    callback(null, true);
  } else {
    callback(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

router.get("/:id", function(req, res) {
  Product.findById(req.params.id, function(err, item) {
    if (err) {
      console.log(err);
      return;
    }
    res.render("edit", {
      item
    });
  });
});

router.post("/:id", upload.single("image"), function(req, res, next) {
  Product.update(
    { _id: req.params.id },
    req.file ? { ...req.body, image: req.file.path } : { ...req.body },
    function(err, result) {
      if (err) {
        res.send(err);
        return;
      }
      res.render("success", {
        title: "Update item success",
        message: "Update item successfully!"
      });
    }
  );
});

module.exports = router;
