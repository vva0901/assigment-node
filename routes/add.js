const express = require("express");
const router = express.Router();
const multer = require("multer");
const fomart = require("../utils/currency");
const Product = require("../models/product");

const storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, "./uploads/");
  },
  filename: function(res, file, callback) {
    callback(null, new Date().getTime() + "-" + file.originalname);
  }
});

const fileFilter = function(req, file, callback) {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    callback(null, true);
  } else {
    callback(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

router.get("/", function(req, res) {
  res.render("add");
});

router.post("/", upload.single("image"), function(req, res, next) {
  let _Product = new Product({
    ...req.body
  });

  _Product.image = req.file.path;

  _Product.save(err => {
    if (err) {
      res.send(err);
      return;
    }
    res.render("success", {
      title: "Add add new product success",
      message: "Add new product successfully!",
      addMoreBtn: true
    });
  });
});

module.exports = router;
