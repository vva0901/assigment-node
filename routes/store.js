var express = require("express");
var router = express.Router();
const product = require("../models/product");
const formart = require('../utils/currency');

/* GET home page. */
router.get("/", function(req, res, next) {
  const query = product.find();
  query.exec(function(err, results) {
    if (err) {
      console.log(err);
      return;
    }
    const all = results.map(value => {
      return {
        name : value.name,
        color: value.color,
        size: value.size,
        gender : value.gender,
        material : value.material,
        pirce :  formart(value.pirce),
        image : value.image
      }
    });
    res.render("store", { listProduct: all });
  });
});

module.exports = router;
